json.extract! product, :id, :name, :description, :price_cents, :images, :weight, :created_at, :updated_at
json.url product_url(product, format: :json)
