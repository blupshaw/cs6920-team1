class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.integer :price_cents
      t.text :images
      t.decimal :weight

      t.timestamps
    end
  end
end
